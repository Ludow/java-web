# Ce dépôt

Dans ce dépôt, vous aurez accès à toutes les ressources utilisées dans les cours et TP sur le développement modulaire d'applications Web avec Java, en utilisant les Servlets, Spring MVC, Spring Boot, Testing, Security, WebFlux, ...

## Semaine 1 - Séance 1 :
Notions de base autour de la programmation Web avec Java
- [Cours 1](./c1_java_web/c1_java_web.pdf)
- [Cours 1 bis](./c1bis_maven/c1_maven.pdf)
- [TP 1](./tp1_java_web/tp1_java_web.pdf)


## Semaine 1 - Séance 2 :
Modulariser les applications Java avec Spring
- [Cours 2](./c2_spring_di/c2_spring_di.pdf)
- [TP 2](./tp2_spring_di/tp2_spring_di.pdf)

## Semaine 2 - Séances 3 et 4 :
Bien structurer une application Web avec Spring MVC
- [Cours 3 et TP 3](./c3_spring_mvc/c3_spring_mvc.pdf)

## Semaine 3 - Séances  5 et 6 :
Auto-configurer une application Web avec Spring Boot
- [Cours 4 et TP 4](./c4_spring_boot/c4_spring_boot.pdf)
- [Script de création des tables de la base de données](./c4_spring_boot/code/create_tables_covid.sql)
- [Dépôt de fichiers](https://moodle.umontpellier.fr/mod/assign/view.php?id=549232)

## Semaine 4 - Séances  7 et 8 :
Sécuriser une application Web avec Spring Security
- [Cours 5 et TP 5](./c5_spring_security/c5_spring_security.pdf)
- [login.jsp](./c5_spring_security/code/login.jsp)
- [register.jsp](./c5_spring_security/code/register.jsp)
- [Dépôt de fichiers](https://moodle.umontpellier.fr/mod/assign/view.php?id=551992)

## Semaine 5 - Séances 9 et 10 :
- [Quiz](https://moodle.umontpellier.fr/mod/quiz/view.php?id=549071)

Tester une application Web avec Spring Testing
- [Cours 6 et TP 6](./c7_spring_testing/c7_spring_testing.pdf)

## Semaine 6 - Séances 11 et 12 :
Définir ses propres modules et service Java 9+
- [Cours 7](./c9_modules/c9_modules_java9.pdf)